# Trix #

This program is the game of "Tic-Tac-Toe" ("Tris" in Italian), developed for the Windows 8 Metro GUI (or Modern applications, or Universal, or whatever name they came up with).

Here is the Windows Store link: https://www.microsoft.com/en-us/store/p/the-trix-tic-tac-toe/9wzdncrdnflg

The project was build with Visual Studio 2012, so you'll need at least that to recompile and run it. Express/Community edition should be fine.

## License ##

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
