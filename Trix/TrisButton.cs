﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Trix {
    class TrisButton : Button {

        public class MoveEventArgs : EventArgs {
            public Position position { get; set; }

            public MoveEventArgs(Position position) {
                this.position = position;
            }
        }

        public delegate void MoveEventHandler(MoveEventArgs args);

        public event MoveEventHandler Move;

        protected virtual void onMove(MoveEventArgs e) {
            if (Move == null) return;
            Move(e);
        }

        public Position position { get; set; }

        public TrisButton(Position position) : base() {
            this.position = position;
            this.Click += cell_Click;
            this.HorizontalAlignment = HorizontalAlignment.Stretch;
            this.VerticalAlignment = VerticalAlignment.Stretch;
            this.FontSize = 40;
            Grid.SetColumn(this, position.col);
            Grid.SetRow(this, position.row);
        }

        void cell_Click(object sender, RoutedEventArgs e) {
            onMove(new MoveEventArgs(position));
        }
    }
}
