﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Trix {
    class Settings {

        private static ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

        private const string LEVEL = "LEVEL";

        public static void saveDefaultLevel(int level) {
            localSettings.Values[LEVEL] = level;
        }

        public static int getDefaultLevel() {
            return localSettings.Values[LEVEL] == null ? 1 : (int)localSettings.Values[LEVEL];
        }
    }
}
