﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Trix.Observerable;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina base è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkId=234237

namespace Trix {
    /// <summary>
    /// Pagina base che fornisce caratteristiche comuni alla maggior parte delle applicazioni.
    /// </summary>
    public sealed partial class GamePage : Trix.Common.LayoutAwarePage, Observer<Dictionary<Sign, int>> {

        MatchManager manager;
        Player pl1, pl2;

        public GamePage() {
            this.InitializeComponent();
            MatchManager.board = new Board(this.trisGrid);            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            base.OnNavigatedTo(e);
            Tuple<Player, Player> players = e.Parameter as Tuple<Player, Player>;
            this.pl1 = players.Item1;
            this.pl2 = players.Item2;
            manager = new MatchManager(this.txtInfo, pl1, pl2);
            txtPl1.Text = pl1.sign.ToString();
            txtPl2.Text = pl2.sign.ToString();
            manager.addObserver(this);
            newMatch();
        }

        private void newGame() {
            MatchManager.game = new Game();
            MatchManager.game.addObserver(MatchManager.board);
            MatchManager.game.addObserver(pl1);
            MatchManager.game.addObserver(pl2);
            MatchManager.board.clear();
            manager.startGame();
        }

        private void newMatch() {
            manager.startMatch();
            btnNewGame.Visibility = Visibility.Collapsed;
            newGame();
        }

        /// <summary>
        /// Popola la pagina con il contenuto passato durante la navigazione. Vengono inoltre forniti eventuali stati
        /// salvati durante la ricreazione di una pagina in una sessione precedente.
        /// </summary>
        /// <param name="navigationParameter">Valore del parametro passato a
        /// <see cref="Frame.Navigate(Type, Object)"/> quando la pagina è stata inizialmente richiesta.
        /// </param>
        /// <param name="pageState">Dizionario di stato mantenuto da questa pagina nel corso di una sessione
        /// precedente. Il valore è null la prima volta che viene visitata una pagina.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState) {
        }

        /// <summary>
        /// Mantiene lo stato associato a questa pagina in caso di sospensione dell'applicazione o se la
        /// viene scartata dalla cache di navigazione. I valori devono essere conformi ai requisiti di
        /// serializzazione di <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">Dizionario vuoto da popolare con uno stato serializzabile.</param>
        protected override void SaveState(Dictionary<String, Object> pageState) {
        }

        private void btnNew_Click(object sender, RoutedEventArgs e) {
            newMatch();
        }

        void Observer<Dictionary<Sign, int>>.update(Dictionary<Sign, int> points) {
            updatePoints(points);
        }

        private void updatePoints(Dictionary<Sign, int> points) {
            txtPointsPl1.Text = points[pl1.sign].ToString();
            txtPointsPl2.Text = points[pl2.sign].ToString();
            btnNewGame.Visibility = Visibility.Visible;
        }

        private void btnNewGame_Click(object sender, RoutedEventArgs e) {
            newGame();
            btnNewGame.Visibility = Visibility.Collapsed;
        }
    }
}
