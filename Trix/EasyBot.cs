﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trix {
    class EasyBot : StupidBot {

        public EasyBot(Sign sign)
            : base(sign) { }

        protected override Position getMove() {
            Position defenseMove = null;
            // cycles all winning positions
            foreach (var wPos in Game.winningPos) {
                // you earn a point with your sign, and you lose with opponents
                int points = getPoint(wPos.p1) + getPoint(wPos.p2) + getPoint(wPos.p3);
                if (points == 2) 
                    //means you have 2 signs on this row and there is an empty space: you can win!!
                    return empty(wPos);
                if (points == -2)
                    //your opponent has 2 signs: lock it, but only at the end; if you can win, you don't need to lock him!
                    defenseMove = empty(wPos);
            }
            //defense
            if (defenseMove != null)
                return defenseMove;
            //if you cannot win or defense, random move!
            return getRandomMove();
        }

        // gets the empty position in a set of three
        private Position empty(Game.TrisPos wPos) {
            if (getPoint(wPos.p1) == 0) return wPos.p1;
            if (getPoint(wPos.p2) == 0) return wPos.p2;
            if (getPoint(wPos.p3) == 0) return wPos.p3;
            throw new ArgumentException("Una delle 3 posizioni deve essere vuota!!");
        }

        //0 means empty, 1 means yours, -1 your opponent
        private int getPoint(Position position) {
            if (MatchManager.game.getSign(position) == null) return 0;
            return MatchManager.game.getSign(position).Equals(sign) ? 1 : -1;
        }
    }
}
