﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trix {
    public class Position {
        public int row { get; set; }
        public int col { get; set; }

        public Position(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType()) {
                return false;
            }
            Position p = (Position)obj;
            return p.col == this.col && p.row == this.row;
        }
    }
}
