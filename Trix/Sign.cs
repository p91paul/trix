﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trix {
    public class Sign {
        public static Sign X = new Sign('X');
        public static Sign O = new Sign('O');
        public char c { get; private set; }

        private Sign(char p) {
            this.c = p;
        }

        public override string ToString() {
            return c.ToString();
        }

    }
}
