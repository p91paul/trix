﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trix.Observerable;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Trix {
    class Board : Observer<Position> {

        private TrisButton[,] cells = new TrisButton[3, 3];

        public HumanPlayer currentPlayer { get; set; }

        public Board(Grid grid) {
            for (int r = 0; r < 3; r++)
                for (int c = 0; c < 3; c++) {
                    cells[r, c] = new TrisButton(new Position(r, c));
                    grid.Children.Add(cells[r, c]);
                    cells[r, c].Move += moveMade;
                }
        }

        public void clear() {
            foreach (var cell in cells) {
                cell.Content = "";
                cell.IsEnabled = true;
            }
        }

        public void win(Trix.Game.TrisPos winning) {
            List<Position> wpos = new List<Position>();
            wpos.Add(winning.p1); wpos.Add(winning.p2); wpos.Add(winning.p3);
            foreach (var cell in cells)
                cell.IsEnabled = wpos.Contains(cell.position);
            currentPlayer = null;
        }

        private void moveMade(TrisButton.MoveEventArgs args) {
            if (currentPlayer == null)
                return;
            currentPlayer.move(args.position);
        }

        void Observer<Position>.update(Position pos) {
            if (pos == null)
                clear();
            else {
                cells[pos.row, pos.col].Content = MatchManager.game.getSign(pos).ToString();
                cells[pos.row, pos.col].IsEnabled = false;
            }
        }
    }
}
