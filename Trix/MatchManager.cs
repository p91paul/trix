﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trix.Observerable;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Controls;

namespace Trix {
    class MatchManager : Observable<Dictionary<Sign, int>> {

        public static Game game { get; set; }
        public static Board board { get; set; }

        public Player pl1 { get; private set; }
        public Player pl2 { get; private set; }

        public Dictionary<Sign, int> points = new Dictionary<Sign,int>();

        private TextBlock txtInfo;

        private Player current, next;

        private bool playing = false;
        private ResourceLoader loader = new ResourceLoader();

        public MatchManager(TextBlock txtInfo, Player p1, Player p2) {
            this.txtInfo = txtInfo;
            this.pl1 = p1;
            this.pl2 = p2;
        }

        public void nextMove() {
            if (playing) {
                var tmp = current;
                current = next;
                next = tmp;
                toCurrent();
            }
        }

        public void startGame() {
            playing = true;
            pl1.startGame();
            pl2.startGame();
            current = pl1;
            next = pl2;
            //invert players for next game
            pl1 = next;
            pl2 = current;
            MatchManager.game.WaitMove += nextMove;
            MatchManager.game.Win += (tpos) => {
                playing = false;
                if (tpos != null) {
                    Sign winning = MatchManager.game.getSign(tpos.tris.p1);
                    txtInfo.Text = string.Format(loader.GetString("wins"),winning.ToString());
                    board.win(tpos.tris);
                    points[winning]++;
                } else
                    txtInfo.Text = loader.GetString("draw");
                notifyObservers(points);
            };
            toCurrent();
        }

        public void startMatch() {
            points[pl1.sign] = points[pl2.sign] = 0;
            notifyObservers(points);
        }

        private void toCurrent() {
            this.txtInfo.Text = string.Format(loader.GetString("turnOf"), current.sign.ToString());
            current.yourTurn();
        }
    }
}
