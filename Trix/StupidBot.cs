﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trix.Observerable;
using Windows.UI.Xaml;

namespace Trix {
    class StupidBot : Player {

        private List<Position> empty = new List<Position>();
        private Random rand = new Random();
        protected DispatcherTimer timer;

        public StupidBot(Sign sign)
            : base(sign) {
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            timer.Tick += (s, a) => {
                move(getMove());
                timer.Stop();
            };

            reset();
        }

        private void reset() {
            empty.Clear();
            for (int r = 0; r < 3; r++)
                for (int c = 0; c < 3; c++)
                    empty.Add(new Position(r, c));

            timer.Stop();
        }

        public override void yourTurn() {            
            timer.Start();
        }

        protected virtual Position getMove() {
            return getRandomMove();
        }

        protected Position getRandomMove() {
            return empty[rand.Next(empty.Count)];
        }


        public override void update(Position modification) {
            empty.Remove(modification);
        }

        public override void startGame() {
            reset();
        }
    }
}
