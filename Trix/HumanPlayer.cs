﻿using System;

namespace Trix {
    public class HumanPlayer : Player {

        public HumanPlayer(Sign sign)
            : base(sign) {
        }

        public override void yourTurn() {
            MatchManager.board.currentPlayer = this;
        }
    }
}