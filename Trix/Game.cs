﻿using System;
using System.Collections.Generic;
using Trix.Observerable;
namespace Trix {
    public class Game : Observable<Position> {

        public class TrisPos {
            public Position p1 { get; private set; }
            public Position p2 { get; private set; }
            public Position p3 { get; private set; }
            public TrisPos(Position p1, Position p2, Position p3) {
                this.p1 = p1;
                this.p2 = p2;
                this.p3 = p3;
            }

        }

        // wait for move event
        public delegate void WaitEventHandler();
        public event WaitEventHandler WaitMove;
        protected virtual void onWaitMove() {
            if (WaitMove == null) return;
            WaitMove();
        }

        // win event
        public class WinEventArgs : EventArgs {
            public TrisPos tris { get; set; }

            public WinEventArgs(TrisPos tris) {
                this.tris = tris;
            }
        }

        public delegate void WinEventHandler(WinEventArgs args);

        public event WinEventHandler Win;

        protected virtual void onWin(WinEventArgs e) {
            if (Win == null) return;
            Win(e);
        }

        public static ICollection<TrisPos> winningPos = fillWinningPos();

        private Sign[,] signs = new Sign[3,3];
        private int free = 9;

        private static ICollection<TrisPos> fillWinningPos() {
            ICollection<TrisPos> winningPos = new LinkedList<TrisPos>();
            for (int i = 0; i < 3; i++) {
                winningPos.Add(new TrisPos(new Position(i, 0), new Position(i, 1), new Position(i, 2)));
                winningPos.Add(new TrisPos(new Position(0, i), new Position(1, i), new Position(2, i)));
            }
            winningPos.Add(new TrisPos(new Position(0, 0), new Position(1, 1), new Position(2, 2)));
            winningPos.Add(new TrisPos(new Position(0, 2), new Position(1, 1), new Position(2, 0)));
            return winningPos;
        }

        public Sign getSign(Position pos){
            return signs[pos.row,pos.col];
        }

        public void putSign(Position position, Sign sign) {
            if (signs[position.row, position.col] != null)
                throw new ArgumentException("Position already filled");
            signs[position.row, position.col] = sign;
            free--;
            notifyObservers(position);
            bool win = checkWin();
            if (!win) {
                if (free == 0)
                    Win(null);
                else
                    onWaitMove();
            }
        }

        private bool checkWin() {
            foreach (var wp in winningPos) {
                Sign s = getSign(wp.p1);
                if (s!=null && s == getSign(wp.p2) && s == getSign(wp.p3)) {
                    onWin(new WinEventArgs(wp));
                    return true;
                }
            }
            return false;
        }
    }
}