﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina base è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkId=234237

namespace Trix {
    /// <summary>
    /// Pagina base che fornisce caratteristiche comuni alla maggior parte delle applicazioni.
    /// </summary>
    public sealed partial class MainPage : Trix.Common.LayoutAwarePage {
        ResourceLoader loader = new ResourceLoader();

        public MainPage() {
            this.InitializeComponent();
            cmbLevel.SelectedIndex = Settings.getDefaultLevel();
            SettingsPane.GetForCurrentView().CommandsRequested += OnSettingsPaneCommandRequested;
        }

        public void OnSettingsPaneCommandRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args) {
#pragma warning disable //async operations
            args.Request.ApplicationCommands.Clear();
            args.Request.ApplicationCommands.Add(new SettingsCommand("privacypolicy",
                loader.GetString("privacyPolicy"), (x) => {
                    Launcher.LaunchUriAsync(new Uri(loader.GetString("privacyUrl")));
                }));
            args.Request.ApplicationCommands.Add(new SettingsCommand("website",
                loader.GetString("webSite"), (x) => {
                    Launcher.LaunchUriAsync(new Uri(loader.GetString("webUrl")));
                }));
#pragma warning restore
        }

        /// <summary>
        /// Popola la pagina con il contenuto passato durante la navigazione. Vengono inoltre forniti eventuali stati
        /// salvati durante la ricreazione di una pagina in una sessione precedente.
        /// </summary>
        /// <param name="navigationParameter">Valore del parametro passato a
        /// <see cref="Frame.Navigate(Type, Object)"/> quando la pagina è stata inizialmente richiesta.
        /// </param>
        /// <param name="pageState">Dizionario di stato mantenuto da questa pagina nel corso di una sessione
        /// precedente. Il valore è null la prima volta che viene visitata una pagina.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState) {
        }

        /// <summary>
        /// Mantiene lo stato associato a questa pagina in caso di sospensione dell'applicazione o se la
        /// viene scartata dalla cache di navigazione. I valori devono essere conformi ai requisiti di
        /// serializzazione di <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">Dizionario vuoto da popolare con uno stato serializzabile.</param>
        protected override void SaveState(Dictionary<String, Object> pageState) {
        }

        private void startMatch(Player pl1, Player pl2) {
            Frame.Navigate(typeof(GamePage), new Tuple<Player, Player>(pl1, pl2));
        }

        private async void btnSingle_Click(object sender, RoutedEventArgs e) {
            Player pl1 = new HumanPlayer(Sign.X);
            Player pl2;
            if (cmbLevel.SelectedItem == selStupid)
                pl2 = new StupidBot(Sign.O);
            else if (cmbLevel.SelectedItem == selEasy)
                pl2 = new EasyBot(Sign.O);
            else {
                MessageDialog dialog = new MessageDialog(loader.GetString("mustChooseLevel"));
                await dialog.ShowAsync();
                return;
            }
            Settings.saveDefaultLevel(cmbLevel.SelectedIndex);
            startMatch(pl1, pl2);
        }

        private void btnMulti_Click(object sender, RoutedEventArgs e) {
            Player pl1 = new HumanPlayer(Sign.X);
            Player pl2 = new HumanPlayer(Sign.O);
            startMatch(pl1, pl2);
        }
    }
}
