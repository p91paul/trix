﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trix.Observerable {
    public class Observable<T> {
        private ICollection<Observer<T>> observers = new LinkedList<Observer<T>>();

        public void addObserver(Observer<T> observer) {
            observers.Add(observer);
        }

        public void notifyObservers(T modification) {
            foreach (var observer in observers) 
                observer.update(modification);
        }
    }
}
