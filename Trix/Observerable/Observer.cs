﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trix.Observerable {
    public interface Observer<T> {
        void update(T modification);
    }
}
