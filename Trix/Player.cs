﻿using System;
using Trix.Observerable;
namespace Trix {
    public abstract class Player : Observer<Position> {

        public Sign sign { get; private set; }

        public Player(Sign sign) {
            this.sign = sign;
        }


        public void move(Position position) {
            MatchManager.game.putSign(position, sign);
        }

        public abstract void yourTurn();

        public virtual void update(Position modification) { }

        public virtual void startGame() { }
    }
}